module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: "https://www.xxx/atoolsbe",
        pathRewrite: {
          "^/api": "",
        },
        changeOrigin: true,
      },
    },
  },
  configureWebpack: {
    resolve: {
      alias: {
        components: "@/components",
      },
    },
  },
};
